'use strict';

let balances = {};

/**
 * GET /api/balance/:id
 * Get current balance for id.
 */
const getBalance = (req, res, next) => {
  const id = req.params.id;
  const balance = balances[id] || 0.00;
  res.send(balance.toFixed(2));
};

/**
 * PUT /api/balance/:id
 * Update current balance for id.
 */
const updateBalance = (req, res, next) => {
  const id = req.params.id;
  const amount = req.params.amount;
  const verb = req.params.verb;

  balances[id] = balances[id] || 0.00;

  if (verb == 'add') {
    balances[id] += Number.parseFloat(amount);
  } else if (verb == 'subtract') {
    balances[id] -= Number.parseFloat(amount);
  } else {
    balances[id] = Number.parseFloat(amount);
  }
  next();
};

module.exports = {
  getBalance: getBalance,
  updateBalance: updateBalance,
};
