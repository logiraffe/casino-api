'use strict';

// Load environment variables from .env file, where API keys and passwords are configured.
const dotenv = require('dotenv').config();

const express = require('express');
const http = require('http');
const chalk = require('chalk');
const compression = require('compression');
const errorHandler = require('errorhandler');
const lusca = require('lusca');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');

const app = express();
const server = http.Server(app);
const routes = require('./config').routes;


app.set('port', process.env.PORT || 3000);
app.use(compression());
app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));
app.use(bodyParser.json());
app.use(expressValidator());


/*
 * Express Routes
 */
app.use('/', routes);
app.use(function (err, req, res, next) {
   res.json({error: (err.message)});
   next(err);
})


// development only
if (app.get('env') == 'development') {
  app.use(errorHandler());
}

/**
 * Start Express server.
 */
server.listen(app.get('port'), () => {
  console.log('%s Express server listening on port %d in %s mode.', chalk.green('✓'), app.get('port'), app.get('env'));
});
