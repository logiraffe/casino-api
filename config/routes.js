const router = require('express').Router();
const balanceHandler = require('../handlers').route.balance;

router.all('/api/*', (req, res, next) => {
   // check if token sent in is valid
   next();
});

router.route('/api/balance/:id')
  .get(balanceHandler.getBalance);

router.route(
  '/api/balance/:id/:verb/:amount'
).put(
  balanceHandler.updateBalance,
  balanceHandler.getBalance
);

module.exports = router;
