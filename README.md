Casino API

### Requirements:
* Node >= 6
* Yarn >= 1 or NPM >= 3

### Setup:
1. `cp .env.example .env` and set configs
2. `yarn install` or `npm install`
3. `yarn run dev` or `npm run dev`
  
### Existing API calls
|Description|Method|Endpoint|curl example|
|-------|-------|------------|---------|
|Get balance                |GET    |`/api/balance/:id`|`curl -X GET http://localhost:3000/api/balance/1`
|Set balance to value       |PUT    |`/api/balance/:id/set/:amount`|`curl -X PUT "http://localhost:3000/api/balance/1/set/5"`|
|Add value to balance       |PUT    |`/api/balance/:id/add/:amount`|`curl -X PUT "http://localhost:3000/api/balance/1/add/5"`|
|Subtract value from balance|PUT    |`/api/balance/:id/subtract/:amount`|`curl -X PUT "http://localhost:3000/api/balance/1/subtract/5"`|
